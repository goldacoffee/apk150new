<div>
  <div class="row">
    <div class="col-12">
      <div class="form-group">
        <button class="btn btn-block btn-primary" wire:click="save">SAVE</button>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12 col-md-6 col-lg-6">
      <div class="card">
        <div class="card-header">
          <h4>Channel</h4>
          <div class="card-header-action">
            <a class="btn btn-icon btn-success" href="#" wire:click="addChannel"><i class="fas fa-plus"></i> Add</a>

          </div>

        </div>


      </div>
    </div>
    <div class="col-12 col-md-6 col-lg-6">
      <div class="card">
        <div class="card-header">
          <h4>Tipster</h4>
          <div class="card-header-action">

            <a class="btn btn-icon btn-success" href="#" wire:click="addBet"><i class="fas fa-plus"></i> Add</a>
          </div>

        </div>


      </div>
    </div>
  </div>
  <div class="row">



    <div class="col-12 col-md-6 col-lg-6">
      @foreach ($chList as $key => $item)
        <div class="card">
          <div class="card-header">
            <h4>Channel {{ $key + 1 }}</h4>
            <div class="card-header-action">
              <a class="btn btn-icon btn-danger" href="#" wire:click="removeChannel({{ $key }})"><i
                  class="fas fa-minus"></i></a>
            </div>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-4">
                <div class="form-group">
                  <label>Type</label>
                  <select class="form-control" wire:model="chList.{{ $key }}.type">
                    <option value="hls">HLS</option>
                    <option value="iframe">IFRAME</option>
                    <option value="mpd">MPD</option>
                  </select>
                </div>

              </div>
              <div class="col-8">
                <label for="source">Source</label>
                <div class="form-group">
                  <input type="text" class="form-control" wire:model="chList.{{ $key }}.source">
                </div>
              </div>
            </div>

          </div>
        </div>

      @endforeach


    </div>
    <div class="col-12 col-md-6 col-lg-6">
      @foreach ($betList as $key => $item)
        <div class="card">
          <div class="card-header">
            <h4>Tips {{ $key + 1 }}</h4>
            <div class="card-header-action">
              <a class="btn btn-icon btn-warning" href="#" wire:click="removeBet({{ $key }})"><i
                  class="fas fa-minus"></i></a>
            </div>

          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-4">
                <div class="form-group">
                  <label>Pick Your Color</label>
                  <div class="input-group colorpickerinput">
                    <input type="text" class="form-control" wire:model="betList.{{ $key }}.color"
                      placeholder="#HEXCOLOR">
                    <div class="input-group-append">
                      <div class="input-group-text">
                        <i class="fas fa-fill-drip"></i>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              <div class="col-8">
                <label for="source">Text</label>
                <div class="form-group">
                  <input type="text" class="form-control" wire:model="betList.{{ $key }}.text">
                </div>
              </div>
            </div>

          </div>


        </div>
      @endforeach

    </div>
  </div>


</div>



@push('css')
  <link rel="stylesheet" href="/assets/modules/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
@endpush

@push('js')
  <script src="/assets/modules/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
  <script>
    $(function() {
      $(".colorpickerinput").colorpicker({
        format: 'hex',
        component: '.input-group-append',
      });
    });

    document.addEventListener('DOMContentLoaded', () => {
      this.livewire.on('simplealert', data => {
        swal(data.text);
      })
    })

  </script>
@endpush
