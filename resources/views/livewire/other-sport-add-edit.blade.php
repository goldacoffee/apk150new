<div>
  {{-- Knowing others is intelligence; knowing yourself is true wisdom. --}}
  <div class="row">


    <div class="col-12 col-md-6 col-lg-6">
      <div class="card">
        <div class="card-header">
          <h4>{{ $cardHeaderTitle }}</h4>
        </div>
        <div class="card-body">

          <form action="" wire:submit.prevent="save">
            <div class="form-group">

              <input type="hidden" wire:model="otherId" class="form-control">
            </div>

            <div class="form-group">
              <label for="name">Sport Name</label>
              <input type="text" wire:model="name" class="form-control">
              @error('name')
                <div class="invalid-feedback d-block">
                  {{ $message }}
                </div>
              </span> @enderror
            </div>
            <div class="form-group">
              <label for="name">Slug</label>
              <input type="text" wire:model="slug" class="form-control">
            </div>
            <div class="form-group">
              <label for="name">Sport logo</label>
              <input type="text" wire:model="logo" class="form-control">
            </div>
            <button class="btn btn-block btn-primary">SAVE</button>
          </form>
        </div>
      </div>

    </div>

  </div>



</div>
