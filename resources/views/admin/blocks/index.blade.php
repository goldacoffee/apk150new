@extends('admin.layout')
@section('content-header')
  <h1>Blok Match</h1>
@endsection

@section('content-body')
  <div class="row">


    <div class="col-12 col-md-12 col-lg-12">
      @if (session('error'))
        <div class="alert alert-warning alert-dismissible show fade">
          <div class="alert-body">
            <button class="close" data-dismiss="alert">
              <span>&times;</span>
            </button>
            {{ session('error') }}
          </div>
        </div>
      @endif
      <div class="card">

        <div class="card-header">
          <h4>Blocked Match</h4>
          <div class="card-header-action">
            <a href="{{ route('admin.sportdata.block.add', $type) }}" class="btn btn-icon btn-primary"><i
                class="fas fa-plus"></i> ADD</a>
          </div>
        </div>
        <div class="card-body">

          <div class="table-responsive">
            <table class="table table-striped" id="block-table">
              <thead>
                <tr>

                  <th>Parameter</th>

                  <th class="text-center">Action</th>

                </tr>
              </thead>
              <tbody>
                @foreach ($data as $block)
                  <tr>

                    <td>{{ $block->string }}</td>

                    <td class="text-center">
                      <a href="{{ route('admin.sportdata.block.add', ['id' => $block->id, 'slug' => $type]) }}"
                        class="btn btn-sm btn-icon icon-left btn-primary"><i class="far fa-edit"></i> Edit</a>

                      <a href="{{ route('admin.sportdata.block.delete', ['id' => $block->id, 'slug' => $type]) }}"
                        class="btn btn-sm btn-icon icon-left btn-danger" id="delete-team" "><i class=" fa fa-times"></i>
                        Delete</a>

                    </td>
                  </tr>
                @endforeach




              </tbody>
            </table>

          </div>



        </div>
      </div>
    </div>
  </div>




@endsection


@push('css')
  <link rel="stylesheet" href="/assets/modules/datatables/datatables.min.css">
  <link rel="stylesheet" href="/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">

@endpush

@push('js')
  <!-- JS Libraies -->
  <script src="/assets/modules/datatables/datatables.min.js"></script>
  <script src="/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js"></script>
  <script src="/assets/modules/jquery-ui/jquery-ui.min.js"></script>
  <script src="/assets/modules/jquery-sortable-list/jquery-sortable-lists.min.js"></script>

  <!-- Page Specific JS File -->
  <script>
    $("#block-table").dataTable({
      "columnDefs": [{
        "sortable": true,
        "targets": [0]
      }]
    });

  </script>

@endpush
