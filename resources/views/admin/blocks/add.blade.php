@extends('admin.layout')
@section('content-header')
  <h1>Add New Block {{ $type }}</h1>
@endsection

@section('content-body')
  <div class="row">


    <div class="col-12 col-md-6 col-lg-6 m-auto">
      @if (session('error'))
        <div class="alert alert-warning alert-dismissible show fade">
          <div class="alert-body">
            <button class="close" data-dismiss="alert">
              <span>&times;</span>
            </button>
            {{ session('error') }}
          </div>
        </div>
      @endif
      <div class="card">

        <div class="card-header">
          <h4>Add New Block {{ $type }}</h4>

        </div>
        <div class="card-body">

          <form action="{{ route('admin.sportdata.block.add.do', $type) }}" method="POST">
            @csrf

            <div class="form-group">
            
              <input type="hidden" class="form-control" name="id" id="id"
                value="{{ isset($data->id) ? $data->id : Str::uuid() }}">
            </div>
            <div class="form-group">
              <label for="string">Input Parameter</label>
              <input type="text" class="form-control" name="string" id="string"
                value="{{ isset($data->id) ? $data->string : '' }}">
            </div>

            <div class="form-group">
              <input type="submit" value="SAVE" class="form-control btn btn-primary">
            </div>

          </form>



        </div>
      </div>
    </div>
  </div>




@endsection
