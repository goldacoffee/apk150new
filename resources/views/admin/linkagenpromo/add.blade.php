@extends('admin.layout')
@section('content-header')
    <h1>Add Link Agen Promo</h1>
@endsection

@section('content-body')
    @livewire('add-linkagenpromo',['editId'=>$editId])
@endsection

@push('css')
    <link rel="stylesheet" href="/assets/modules/select2/dist/css/select2.min.css">
@endpush
