@extends('admin.layout')
@section('content-header')
  <h1></h1>
@endsection

@section('content-body')
  <div class="row">
    <div class="col-12 col-md-6 col-lg-12">
      @if (session('notice'))
        <div class="alert alert-success alert-dismissible show fade">
          <div class="alert-body">
            <button class="close" data-dismiss="alert">
              <span>&times;</span>
            </button>
            {{ session('notice') }}
          </div>
        </div>
      @endif
      <div class="card">
        <div class="card-header">
          <h4></h4>
          <div class="card-header-action">
            <a href="{{ route('admin.othersports.add') }}" class="btn btn-icon btn-primary" href="#"><i
                class="fas fa-plus"></i> ADD</a>
            <button class="btn btn-icon btn-success" id="save-othersport-position"><i class=" fas fa-plus"></i>
              SAVE</button>

          </div>
        </div>
        <div class="card-body">
          <table class="table table-striped table-dark">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Slug</th>
                <th scope="col" class="text-center">Logo</th>
                <th scope="col" class="text-center">Position</th>
                <th scope="col" class="text-center">Action</th>
              </tr>
            </thead>
            <tbody id="other-sport-list">

              @foreach ($sportlist as $key => $item)
                <tr id="{{ $item->id }}">
                  <td>#{{ $key + 1 }}</td>
                  <td>{{ $item->name }}</td>
                  <td>{{ $item->slug }}</td>
                  <td>
                    <img src="{{ $item->logo }}" alt="{{ $item->name }}" height="40">
                  </td>
                  <td>{{ $item->position }}</td>
                  <td class="text-center">
                    <a href="{{ route('admin.othersports.edit', ['otherId' => $item->id]) }}"
                      class="btn btn-sm btn-icon icon-left btn-primary"><i class="far fa-edit"></i> Edit</a>
                    <a href="{{ route('admin.othersports.delete', ['otherId' => $item->id]) }}"
                      class="btn btn-sm btn-icon icon-left btn-danger"><i class="fas fa-times"></i> Delete</a>


                  </td>
                </tr>

              @endforeach


            </tbody>
          </table>


        </div>
      </div>
    </div>
  @endsection


  @push('css')
    <link rel="stylesheet" href="/assets/modules/datatables/datatables.min.css">
    <link rel="stylesheet" href="/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">

  @endpush

  @push('js')
    <!-- JS Libraies -->
    <script src="/assets/modules/datatables/datatables.min.js"></script>
    <script src="/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script src="/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js"></script>
    <script src="/assets/modules/jquery-ui/jquery-ui.min.js"></script>
    <script src="/assets/modules/jquery-sortable-list/jquery-sortable-lists.min.js"></script>

    <!-- Page Specific JS File -->
    <script>
      $("#other-sport-list").sortable({});
    //   $("#team-table").dataTable({
    //     "columnDefs": [{
    //       "sortable": true,
    //       "targets": [0]
    //     }]
    //   });

    </script>

  @endpush
