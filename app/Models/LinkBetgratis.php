<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LinkBetgratis extends Model
{
    protected $table = 'link_betgratis';
    protected $fillable = [
        'id',
        'judul',
        'image',
        'kalimat',
        'urls',

    ];
    protected $primaryKey = 'id'; // or null
    public $incrementing = false;


    protected $casts = [
        'urls' => 'array',
    ];
}
