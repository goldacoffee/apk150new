<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipsterArea extends Model
{
    protected $table = 'tipster_area';
    protected $fillable = [
        'id',
        'channel',
        'bet',

    ];
    protected $primaryKey = 'id'; // or null
    public $incrementing = false;

    protected $casts = [
        'channel' => 'array',
        'bet' => 'array',
    ];
}
