<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SportMatch extends Model
{
    protected $table = 'match';
    protected $fillable = [
        'id',
        'liga',
        'slug',
        'home',
        'away',
        'is_request',
        'sport',
        'position',
        'sources',

    ];
    protected $primaryKey = 'id'; // or null
    public $incrementing = false;


    protected $casts = [
        'sources' => 'array',
    ];

   
    public function homedata()
    {
        return $this->hasOne(Team::class, 'id', 'home');
    }
    public function awaydata()
    {
        return $this->hasOne(Team::class, 'id', 'away');
    }
}
