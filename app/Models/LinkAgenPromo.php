<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LinkAgenPromo extends Model
{
    public $incrementing = false;
    protected $table = 'link_agenpromo';
    protected $fillable = [
        'id',
        'judul',
        'image',
        'kalimat',
        'urls',
    ];
    protected $primaryKey = 'id'; // or null

    protected $casts = [
        'urls' => 'array',
    ];
}
