<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TipsterareaController extends Controller
{
    public function index()
    {
        setActiveMenu('othersports');
        setTitle('Other Sports');

        return view('admin.tipsterarea.index');
    }

    public function delete($otherId)
    {
        setActiveMenu('othersports');
        setTitle('Other Sports Confirm Delete');

        return view('admin.tipsterarea.delete-confirm');
    }

    public function doDelete($otherId, Request $request)
    {
    }
}
