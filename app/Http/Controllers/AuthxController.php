<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class AuthxController extends Controller
{
    public function login()
    {

        return view('auth.login');
    }

    public function doLogin(Request $request)
    {
        $credentials =  $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            
            $request->session()->regenerate();
            return redirect(route('admin.index'));
        } else {
            return redirect()->back()->with('error', 'Invalid email or password');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect(route('do.login'));
    }
}
