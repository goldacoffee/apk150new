<?php

namespace App\Http\Controllers;

use App\Models\LinkAgenPromo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LinkAgenPromoC extends Controller
{
    public function index()
    {
        setActiveMenu('linkagenpromo');
        $data = DB::table('link_agenpromo')->orderBy('position', 'asc')->get();

        return view('admin.linkagenpromo.index', ['data' => $data]);
    }

    public function addLink(Request $request)
    {
        if (isset($request->id)) {
            $editId = $request->id;
        } else {
            $editId = 'new';
        }

        return view('admin.linkagenpromo.add', [
            'editId' => $editId,
        ]);
    }

    public function confirmDelete(Request $request)
    {
        $data = LinkAgenPromo::find($request->id);

        return view('admin.linkagenpromo.confirm-delete', ['data' => $data]);
    }

    public function doDelete($id, Request $request)
    {
        // dd($request);
        $delete = destroyLinkAgenpromo($id);
        $data = LinkAgenPromo::orderBy('position')->get();
        saveJson('link-agenpromo', $data);

        if (true === $delete) {
            return redirect(route('admin.linkagenpromo'))->with('notice', 'Data delete successfully');
        }

        return redirect()->back()->with('error', 'Data delete error, contact admin!!!');
    }
}
