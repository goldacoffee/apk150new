<?php

namespace App\Http\Controllers;

use App\Models\LinkBetgratis;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LinkBetgratisController extends Controller
{
    public function index()
    {
        setActiveMenu('linkBetgratis');
        $data = DB::table('link_betgratis')->orderBy('position', 'asc')->get();
        return view('admin.linkbetgratis.index', ['data' => $data]);
    }

    public function addLink(Request $request)
    {

        if (isset($request->id)) {
            $editId =  $request->id;
        } else {
            $editId = 'new';
        }


        return view('admin.linkbetgratis.add', [
            'editId' => $editId
        ]);
    }

    public function confirmDelete(Request $request)
    {

        $data = LinkBetgratis::find($request->id);
        return view('admin.linkbetgratis.confirm-delete', ['data' => $data]);
    }

    public function doDelete($id, Request $request)
    {
        $delete = destroyLinkBetgratis($id);
        $data   = LinkBetgratis::orderBy('position')->get();
        saveJson('link-betgratis', $data);

        if ($delete === true) {
            return redirect(route('admin.linkbetgratis'))->with('notice', 'Data delete successfully');
        } else {
            return redirect()->back()->with('error', "Data delete error, contact admin!!!");
        }
    }
}
