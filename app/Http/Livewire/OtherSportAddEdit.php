<?php

namespace App\Http\Livewire;

use Illuminate\Support\Str;

use Livewire\Component;
use App\Models\OtherSports;

class OtherSportAddEdit extends Component
{
    public $cardHeaderTitle;

    public $otherId = null;


    public $name, $logo, $slug, $position;

    protected $rules = [
        'name' => 'required',

    ];

    protected $messages = [
        'name.required' => "Sport name can't be empty"
    ];

    public function save()
    {
        $this->validate();


        $otherSport = $this->otherId == 'new' ? new OtherSports(['id' => Str::uuid()]) : OtherSports::find($this->otherId);
        $otherSport->name =  $this->name;
        $otherSport->slug = $this->slug == null ? Str::slug($this->name) : $this->slug;
        $otherSport->logo = $this->logo;
        $otherSport->position  = $this->position;
        $otherSport->save();

        saveJson(($this->slug == null) ? 'othersport-' . Str::slug($this->name) : 'othersport-' . $this->slug, $otherSport);

        writeOtherSport();
        return redirect(route('admin.othersports.index'));
    }

    public function mount()
    {
        if ($this->otherId == null) {
            $this->otherId =  'new';
            $this->cardHeaderTitle = "Add new other sport data";
        } else {

            $otherSportData = OtherSports::find($this->otherId);
            $this->name  = $otherSportData->name;
            $this->slug  = $otherSportData->slug;
            $this->logo  = $otherSportData->logo;
            $this->cardHeaderTitle = "Editing : " . $otherSportData->name;
        }
    }
    public function render()
    {
        return view('livewire.other-sport-add-edit')->extends('admin.layout')->section('content-body');
    }
}
