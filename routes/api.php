<?php

use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::POST('/save-match-position', [ApiController::class, 'changeMatchPosition']);
Route::POST('/save-linkbetgratis-position ', [ApiController::class, 'changeLinkBetgratisPosition']);
Route::POST('/save-linkagenpromo-position ', [ApiController::class, 'changeLinkAgenPromoPosition']);
Route::POST('/save-othersport-position ', [ApiController::class, 'changeOtherSportPosition']);
Route::GET('/cron-delete-live ', [ApiController::class, 'cronDeleteLive']);
