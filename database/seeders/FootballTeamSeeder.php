<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class FootballTeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            ["Manchester United", "https://ssl.gstatic.com/onebox/media/sports/logos/udQ6ns69PctCv143h-GeYw_48x48.png"],
            ["Fulham", "https://ssl.gstatic.com/onebox/media/sports/logos/Gh7_5p3n364p4vxeM8FhNg_48x48.png"],
            ["Southampton", "https://ssl.gstatic.com/onebox/media/sports/logos/y1V4sm2SEBiWUPRIYl5rfg_48x48.png"],
            ["Leeds United", "https://ssl.gstatic.com/onebox/media/sports/logos/5dqfOKpjjW6EwTAx_FysKQ_48x48.png"],
            ["Brighton Albion", "https://ssl.gstatic.com/onebox/media/sports/logos/EKIe0e-ZIphOcfQAwsuEEQ_48x48.png"],
            ["Manchester City", "https://ssl.gstatic.com/onebox/media/sports/logos/z44l-a0W1v5FmgPnemV6Xw_48x48.png"],
            ["Chelsea", "https://ssl.gstatic.com/onebox/media/sports/logos/fhBITrIlbQxhVB6IjxUO6Q_48x48.png"],
            ["Leicester City", "https://ssl.gstatic.com/onebox/media/sports/logos/UDYY4FSlty6fXFBzvFfcyw_48x48.png"],
            ["Everton", "https://ssl.gstatic.com/onebox/media/sports/logos/C3J47ea36cMBc4XPbp9aaA_48x48.png"],
            ["Wolves", "https://ssl.gstatic.com/onebox/media/sports/logos/-WjHLbBIQO9xE2e2MW3OPQ_48x48.png"],
            ["Newcastle United", "https://ssl.gstatic.com/onebox/media/sports/logos/96CcNNQ0AYDAbssP0V9LuQ_48x48.png"],
            ["Sheffield United", "https://ssl.gstatic.com/onebox/media/sports/logos/wF8AgQsssfy3_GLyVR3dSg_48x48.png"],
            ["Tottenham Hotspur", "https://ssl.gstatic.com/onebox/media/sports/logos/k3Q_mKE98Dnohrcea0JFgQ_48x48.png"],
            ["Aston Villa", "https://ssl.gstatic.com/onebox/media/sports/logos/uyNNelfnFvCEnsLrUL-j2Q_48x48.png"],
            ["Crystal Palace", "https://ssl.gstatic.com/onebox/media/sports/logos/8piQOzndGmApKYTcvyN9vA_48x48.png"],
            ["Arsenal", "https://ssl.gstatic.com/onebox/media/sports/logos/4us2nCgl6kgZc0t3hpW75Q_48x48.png"],
            ["Burnley", "https://ssl.gstatic.com/onebox/media/sports/logos/teLLSaMXim_8BA1d93sMng_48x48.png"],
            ["Liverpool", "https://ssl.gstatic.com/onebox/media/sports/logos/0iShHhASp5q1SL4JhtwJiw_48x48.png"],
            ["West Brom", "https://ssl.gstatic.com/onebox/media/sports/logos/Im2UqFKvfm3TaM7R2RYkjw_48x48.png"],
            ["West Ham", "https://ssl.gstatic.com/onebox/media/sports/logos/bXkiyIzsbDip3x2FFcUU3A_48x48.png"],
            ["Levante", "https://ssl.gstatic.com/onebox/media/sports/logos/SQB-jlVosxVV1Ce79FhbOA_48x48.png"],
            ["Cadiz", "https://ssl.gstatic.com/onebox/media/sports/logos/H1vdTrUUqJBeUW1HfbW0nQ_48x48.png"],
            ["Huesca", "https://ssl.gstatic.com/onebox/media/sports/logos/S72MCTiOGlQRhTfHW0uf9A_48x48.png"],
            ["Valencia", "https://ssl.gstatic.com/onebox/media/sports/logos/QPbjvDwI_0Wuu4tCS2O6uw_48x48.png"],
            ["Valladolid", "https://ssl.gstatic.com/onebox/media/sports/logos/HlIrXZRP96tv0H1uiiN0Jg_48x48.png"],
            ["Atletico Madrid", "https://ssl.gstatic.com/onebox/media/sports/logos/srAAE0bOnCppUrlbJpFiHQ_48x48.png"],
            ["Real Madrid", "https://ssl.gstatic.com/onebox/media/sports/logos/Th4fAVAZeCJWRcKoLW7koA_48x48.png"],
            ["Villarreal", "https://ssl.gstatic.com/onebox/media/sports/logos/WKH7Ak5cYD6Qm1EEqXzmVw_48x48.png"],
            ["Elche", "https://ssl.gstatic.com/onebox/media/sports/logos/uyyqqxLIYT_lQIXRyMI_RA_48x48.png"],
            ["Bilbao", "https://ssl.gstatic.com/onebox/media/sports/logos/OSL_5Zm6kYPP1J17Bo5uDA_48x48.png"],
            ["Celta Vigo", "https://ssl.gstatic.com/onebox/media/sports/logos/wpdhixHP_sloegfP0UlwAw_48x48.png"],
            ["Real Betis", "https://ssl.gstatic.com/onebox/media/sports/logos/S0fDZjYYytbZaUt0f3cIhg_48x48.png"],
            ["Osasuna", "https://ssl.gstatic.com/onebox/media/sports/logos/pk-hNCNpGM_JDoHHvLVF-Q_48x48.png"],
            ["Real Sociedad", "https://ssl.gstatic.com/onebox/media/sports/logos/w8tb1aeBfVZIj9tZXf7eZg_48x48.png"],
            ["Eibar", "https://ssl.gstatic.com/onebox/media/sports/logos/sbTFnFgPeUsTa-szphHf8Q_48x48.png"],
            ["Barcelona", "https://ssl.gstatic.com/onebox/media/sports/logos/paYnEE8hcrP96neHRNofhQ_48x48.png"],
            ["Granada", "https://ssl.gstatic.com/onebox/media/sports/logos/Fn_X2IO4-1ACuTemcHkDEw_48x48.png"],
            ["Getafe", "https://ssl.gstatic.com/onebox/media/sports/logos/1UDHZMdKZD15W5gus7dJyg_48x48.png"],
            ["Sevilla", "https://ssl.gstatic.com/onebox/media/sports/logos/hCTs5EX3WjCMC5Jl3QE4Rw_48x48.png"],
            ["Alaves", "https://ssl.gstatic.com/onebox/media/sports/logos/meAnutdPID67rfUecKaoFg_48x48.png"],
            ["Cagliari", "https://ssl.gstatic.com/onebox/media/sports/logos/e9XfySyGdfyJ4UzEkYwENw_48x48.png"],
            ["Genoa", "https://ssl.gstatic.com/onebox/media/sports/logos/GlyoZO04xkwVX6oTh8asHA_48x48.png"],
            ["Sampdoria", "https://ssl.gstatic.com/onebox/media/sports/logos/E8bC8MBR8FwSP3ONEhMxjw_48x48.png"],
            ["Parma", "https://ssl.gstatic.com/onebox/media/sports/logos/Pr7ZXZlx34eEXdUMTkLvkw_48x48.png"],
            ["Crotone", "https://ssl.gstatic.com/onebox/media/sports/logos/SJTkRPiSPKkhhMcMm_DVSQ_48x48.png"],
            ["Fiorentina", "https://ssl.gstatic.com/onebox/media/sports/logos/5BhAG7vGRyQLIXtfMG3L2Q_48x48.png"],
            ["Inter Milan", "https://ssl.gstatic.com/onebox/media/sports/logos/gpWqqaYc9yESzfkfspryoA_48x48.png"],
            ["Udinese", "https://ssl.gstatic.com/onebox/media/sports/logos/92Aw_iasBENKmzvdpbTpHQ_48x48.png"],
            ["Atalanta", "https://ssl.gstatic.com/onebox/media/sports/logos/0XmrZHobvb6ua5tgMOnTEA_48x48.png"],
            ["ACMilan", "https://ssl.gstatic.com/onebox/media/sports/logos/VoKsJ6RitaHGhsM62e6AXQ_48x48.png"],
            ["Bologna", "https://ssl.gstatic.com/onebox/media/sports/logos/WnKdNmw06v2lz7HjhqPRPw_48x48.png"],
            ["Juventus", "https://ssl.gstatic.com/onebox/media/sports/logos/Lv6xmBlUIpN3GAFhtf6nqQ_48x48.png"],
            ["Sassuolo", "https://ssl.gstatic.com/onebox/media/sports/logos/GoeTFIVAZLA5JWk0-A6B0A_48x48.png"],
            ["Lazio", "https://ssl.gstatic.com/onebox/media/sports/logos/jcKKlUVaNw3br9cIyOKmQA_48x48.png"],
            ["Torino", "https://ssl.gstatic.com/onebox/media/sports/logos/ovE3HSEx4GWXkW8GU7KVhA_48x48.png"],
            ["Benevento", "https://ssl.gstatic.com/onebox/media/sports/logos/LyjvhG7wXe-iVAepsbjIVg_48x48.png"],
            ["Napoli", "https://ssl.gstatic.com/onebox/media/sports/logos/PWRLYBJqlGrAAsKkUN6eng_48x48.png"],
            ["Verona", "https://ssl.gstatic.com/onebox/media/sports/logos/Y23PEIJgTvK3Qpm9il1MGA_48x48.png"],
            ["Spezia", "https://ssl.gstatic.com/onebox/media/sports/logos/UzdHN3YGjZDZGfSMQuZrYw_48x48.png"],
            ["AS Roma", "https://ssl.gstatic.com/onebox/media/sports/logos/BQdP4jUBFJfG7U_JBsFIMg_48x48.png"],
            ["St Etienne", "https://ssl.gstatic.com/onebox/media/sports/logos/k-EMFQteFMhhnEWVch_dBA_48x48.png"],
            ["Dijon", "https://ssl.gstatic.com/onebox/media/sports/logos/Qf3Beb1hzy-ENYzJOSVJ0Q_48x48.png"],
            ["Lyon", "https://ssl.gstatic.com/onebox/media/sports/logos/SrKK55dUkCxe4mJsyshfCg_48x48.png"],
            ["OGC Nice", "https://ssl.gstatic.com/onebox/media/sports/logos/Llrxrqsc3Tw4JzE6xM7GWw_48x48.png"],
            ["Reims", "https://ssl.gstatic.com/onebox/media/sports/logos/NWzvJ-A3j8HQkeQZ0sJP1w_48x48.png"],
            ["Bordeaux", "https://ssl.gstatic.com/onebox/media/sports/logos/qZwZftPoJrwjjhS1xM-36g_48x48.png"],
            ["Angers", "https://ssl.gstatic.com/onebox/media/sports/logos/dl67d4kLGRVAGfdH2jfvDQ_48x48.png"],
            ["Lille", "https://ssl.gstatic.com/onebox/media/sports/logos/D2AQe8qoyPIP4K8MzLvwuA_48x48.png"],
            ["Lens", "https://ssl.gstatic.com/onebox/media/sports/logos/TUvwItKazVFpgThEhhlN1Q_48x48.png"],
            ["Monaco", "https://ssl.gstatic.com/onebox/media/sports/logos/svD-B0kpoDlKfZuiJxMs4Q_48x48.png"],
            ["Rennes", "https://ssl.gstatic.com/onebox/media/sports/logos/guI8eg4hoTyIp6rO1opjxA_48x48.png"],
            ["Nimes", "https://ssl.gstatic.com/onebox/media/sports/logos/PxMdAScr1phMfc-IN8JYcg_48x48.png"],
            ["Brest", "https://ssl.gstatic.com/onebox/media/sports/logos/HNqZwlu71GHXo60XjYrPxg_48x48.png"],
            ["Paris Saint-Germain", "https://ssl.gstatic.com/onebox/media/sports/logos/mcpMspef1hwHwi9qrfp4YQ_48x48.png"],
            ["Strasbourg", "https://ssl.gstatic.com/onebox/media/sports/logos/Eb9xtMpUy8FXQ0RCKvLxcg_48x48.png"],
            ["Loient", "https://ssl.gstatic.com/onebox/media/sports/logos/bbYkAWWtD6lpK5KyGfr1vA_48x48.png"],
            ["Nantes", "https://ssl.gstatic.com/onebox/media/sports/logos/r3qizrAtoAXPICgYjFYCyA_48x48.png"],
            ["Montpellier", "https://ssl.gstatic.com/onebox/media/sports/logos/fL5Nk_2eUanYiOSB9AnBpQ_48x48.png"],
            ["Metz", "https://ssl.gstatic.com/onebox/media/sports/logos/pKiX1s6Rr8IxVQCuwIKKpg_48x48.png"],
            ["Marseille", "https://ssl.gstatic.com/onebox/media/sports/logos/KfBX1kHNj26r9NxpqNaTkA_48x48.png"],
        );
        foreach ($data as $team) {
            DB::table('teams')->insert([
                "id" => Str::slug($team[0]),
                "name" => $team[0],
                "logo" => $team[1],
                "sport" => "football"
            ]);
        }
    }
}
