<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class BlockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // blok liga
        $files = glob(public_path() . '/streamdata/*'); // get all file names
        foreach ($files as $file) { // iterate files
            if (is_file($file)) {
                unlink($file); // delete file
            }
        }

        foreach ($this->blockLigaList() as $key => $value) {
            DB::table('block_liga')->updateOrInsert(['id' => Str::uuid()], ['string' => $value]);
        }
        writeBlock('liga');

        foreach ($this->blockMatchList() as $key => $value) {
            DB::table('block_match')->updateOrInsert(['id' => Str::uuid()], ['string' => $value]);
        }
        writeBlock('match');


        // $files = glob(public_path() . '/streamdata/*'); // get all file names
        // foreach ($files as $file) { // iterate files
        //     if (is_file($file)) {
        //         unlink($file); // delete file
        //     }
        // }
    }

    private function blockMatchList()
    {
        return  $jayParsedAry = [
            "Persib Bandung vs Bali United Pusam",
            "Renown vs Ratnam",
            "Giugliano vs Nocerina"
        ];
    }

    private function blockLigaList()
    {


        return      $jayParsedAry = [
            "4x4",
            "3x3",
            "Iran. Azadegan League",
            "4.",
            "5.",
            "6x6",
            "5x5",
            "1x1",
            "Sudan. Premier League",
            "Cameroon. Elite One",
            "Croatia. 2. HNL",
            "Israel Championship U19",
            "Israel. Liga Alef. North",
            "Olympics. Women",
            "England. Development League U23",
            "6x6. RPL",
            "1x1. Sniper Cup. Free Throw",
            "3С…3. Premier League",
            "Russia. PFL. South",
            "Russian Championship. National Student Football League",
            "3С…3. League 52",
            "Russia. Superleague. Division 2. Women",
            "VTB United League. Youth League",
            "Israel Championship. Women",
            "Indonesia Cup",
            "Argentina. Campeonato de Mayores",
            "Argentina. Torneo Federal",
            "CAF Confederation Cup",
            "Greece. Women League",
            "Montenegro. Prva Crnogorska Liga",
            "Netherlands. Eredivisie. Women",
            "Republic of Malawi. Super League",
            "Palestine. West Bank League",
            "Morocco. Botola",
            "Italy. Lega Pro A",
            "Guatemala. Liga Nacional",
            "Italy. Lega Pro B",
            "Italy. Lega Pro C",
            "Turkey. PTT 1. Lig",
            "Costa Rica. Segunda Division",
            "Brazil. Gaucho Cup U19",
            "Bolivia. Division 2",
            "Peru Cup",
            "WNCAA",
            "Argentina Championship. Women",
            "Argentina. Primera B Metropolitana",
            "Argentina. Primera D. Metropolitana",
            "Colombia. CategorГ­a Primera B",
            "Guatemala. Primera Division de Ascenso",
            "Brazil. LSB. Women",
            "Colombia Liga",
            "Denmark Championships. Women",
            "7x7. La Liga Roja",
            "3х3. League 52",
            "Qatar Championship U23",
            "Kuwait. Federation Cup",
            "Alpe Adria Cup",
            "Bulgaria Championship U19",
            "Serbia. ZSL. Women",
            "Switzerland. LNA",
            "Netherlands. DBL",
            "ABA League. Women",
            "Spain. Segunda Division B. Women",
            "CECAFA Cup",
            "COSAFA Cup U20",
            "Albania. Superliga",
            "Australia Victorian Women's Premier League",
            "Angola. Girabola",
            "CAF Champions League",
            "England. Championship. Women",
            "Israel. Liga Alef. South",
            "Argentina. Primera C Metropolitana",
            "Argentina. Torneo Argentino A",
            "Chile. Tercera Division",
            "Brazil. Campeonato Paulista. Women",
            "France. LFB. Women",
            "Italy. Serie A2",
            "Italy. Serie B",
            "Poland. League 2",
            "Armenia. First League",
            "Spain. Primera Division. Women",
            "Turkey. TFF 3. Lig",
            "Nevskaya Basketbolnaya Liga. Division 1",
            "Greece Championship. Women",
            "Student SuperLeague. Women",
            "Slovakia. Extraliga. Women",
            "Russia. UBA",
            "Greece. League 3",
            "Turkey Championship. Women",
            "Spain. Tercera Division",
            "Spain Championship. Women",
            "Portugal Championship. Women",
            "Ukraine. Higher League",
            "Kosovo Cup",
            "Hungary. NB II",
            "Georgian Cup",
            "England. Superleague. Women",
            "Egypt. Second Division",
            "Bulgaria. Elite Youth Group U19",
            "Germany. Bundesliga. Women",
            "Germany. Oberliga Mittelrhein",
            "Tunisia. Ligue 1",
            "African Cup of Champions. Women",
            "Argentina Championship U19",
            "Armenian Championship",
            "Qatar. QBL",
            "Russia. Premier League. Women",
            "Russia. Superleague. Division 2",
            "Turkey. TBL. Women",
            "Turkey. Super League. Women",
            "Andorra. Primera Division",
            "Ecuador. Division 2",
            "Mexico Championship U20",
            "Iran Championship U23",
            "2x2. Double Cup",
            "Bulgaria. NBL. Women",
            "Czech Republic. Mattoni NBL",
            "Hungary. Division 1. Women",
            "Poland Championship U20",
            "Poland. Ekstraklasa. Women",
            "Russia. Superleague. Division 1. Women",
            "Slovakia. Extraliga",
            "Slovenia. Liga UPC",
            "Iceland. Division 1",
            "France. Pro B",
            "Italy. Serie A. Women",
            "3С…3. Triangle Cup",
            "Argentina. LDD",
            "NBA. G-League",
            "NCAA. Division 2",
            "NCAA. Division 3",
            "Uruguay Championship",
            "Friendlies U20. National Teams",
            "Croatia. A2",
            "Euroleague U20",
            "Kazakhstan Championship",
            "Serbia. League 2",
            "Ukraine. Superleague. Women",
            "VTB Student League",
            "2nd Dragon League. Group A",
            "Buzzer League 4x4",
            "2nd Dragon League. Group B",
            "Copa Sao Paulo de Futebol Junior U20",
            "Copa Sao Paulo de Futebol Junior",
            "Kenya. Premier League",
            "Morocco. Botola 2",
            "Zambia. Super League",
            "2nd Dragon League. Group C",
            "England. Premier League",
            "National Collegiate Athletic Association (NCAA)",
            "Croatia. A1 Liga",
            "Bosnia and Herzegovina. First Division. Women",
            "Bosnia and Herzegovina. First Division",
            "Estonian-Latvian League",
            "France Championship U21",
            "Romania. Liga Nationala. Women",
            "Serbia. BLS",
            "4C...4",
            "Soap Soccer League. Women",
            "Dragon League. National",
            "Short Football 2x2",
            "Czech Republic. League 1. Women",
            "La Liga Roja 7x7",
            "4С…4",
            "Soap Soccer League",
            "Challenger. Ningbo",
            "Dragon League",
            "Teqball Cup",
            "Duo Soccer",
            "Space League. Women",
            "Premier League 3С…3",
            "Pro League. 3С…3",
            "Division 4С…4",
            "Division 4Ñ…4",
            "England. League Cup",
            "Myanmar Championship U21",
            "Sporting Cristal vs Barcelona SC",
            "Copa Libertadores",
            "Coppa Libertadores",
            "Copa. Libertadores",
            "RPL 6x6",
            "Trio Soccer",
            "Serie A8",
            "Nacional League",
            "Short Football 3x3",
            "Atlantic Cup",
            "Student League. Women",
            "Space League",
            "League 52 3С…3",
            "Vietnam Championship U19",
            "Slovakia. Liga 2",
            "Ukraine Championship U21. Women",
            "PRO Soccer League",
            "Ladies League",
            "Galaxy League",
            "Table Soccer League",
            "Cameroon. CUF Cup",
            "FIFA. eSports Battle.",
            "FIFA. Cyber PRO League",
            "Student League",
            "FIFA. LigaPro",
            "FIFA. Cyber",
            "FIFA 20.",
            "CFL.",
            "8x8.",
            "USSR.",
            "Soccer Box 2x2",
            "Lithuania Championship. League 1",
            "6С…6. Ukraine Championship",
            "Latvia. Virsliga",
            "Iceland. 2 Delid",
            "Albanian Cup",
            "FIFA.",
            "PES 2020.",
            "Iceland 1.",
            "Slovenia. League 1",
            "Czech Republic. 4 Liga",
            "Czech Republic. 5 Liga",
            "Icelandic Cup",
            "Czech Republic. 3 Liga",
            "Russia. PFL. Group 1",
            "Russia. PFL. Group 3",
            "Russian Championship. Amateur League. PIFL",
            "Slovenia. 2. SNL",
            "Slovenia. 3. SNL",
            "Р§РµРјРїРёРѕРЅР°С‚ Р‘РѕСЃРЅРёРё Рё Р“РµСЂС†РµРіРѕРІРёРЅС‹. Р’С‚РѕСЂР°СЏ Р»РёРіР°",
            "Germany. Bundesliga",
            "Germany DFB Pokal",
            "Germany. Oberliga Baden-WГјrttemberg",
            "Germany. Regionalliga North",
            "ACL Indoor",
            "UEFA European Championship 2020",
            "UEFA Nations League",
            "China. Super League",
            "Poland. Ekstraklasa",
            "Netherlands. Eredivisie",
            "Belgium. Jupiler League",
            "UEFA Europa League",
            "FIFA World Cup 2022",
            "Brazil. Campeonato Brasileiro B",
            "Esoccer Battle",
            "FIFA. Elite League",
            "FIFA 21.",
            "Cyber NBA",
            "NBA 2K21",
            "KLASK",
            "Р’Рµlarus. Short Football Brest",
            "NBA 2K20",
            "Р’Рµlarus. Short Football D1",
            "Chinese Taipei.",
            "Portugal. Campeonato Nacional de Seniores",
            "Malta. Challenge League",
            "Turkey. TFF 1. Lig",
            "Esoccer Live Arena",
            "Netherlands. KNVB Beker",
            "Indonesia",
            "Russian Championship. FNL",
            "Friendlies. National Teams",
            "Syria Championship U21",
            "Ukraine. Second League",
            "India. Kerala Premier League",
            "Belarus. Premier League. Women",
            "Spain. La Liga",
            "Division 4x4",
            "Division 4х4",
            "4х4"
        ];
    }
}
